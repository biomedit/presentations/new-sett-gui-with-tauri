# new sett GUI with Tauri

Live version: <https://biomedit.gitlab.io/presentations/new-sett-gui-with-tauri>

## Run locally

```bash
git submodule update --init
ln -s reveal.js/{dist,plugin} .
```

Then open `index.html` in your browser.

## Useful Links

- [Reveal.js](https://revealjs.com/)
- https://sphn-dcc.atlassian.net/wiki/spaces/BIOM/pages/39453267/sett+story+of+how+a+snake+became+a+crab
